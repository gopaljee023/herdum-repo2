package herdum.com.herdum2.app;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import herdum.com.herdum2.R;
import herdum.com.herdum2.adapter.ContactRecyclerAdapter;
import herdum.com.herdum2.model.Contact;

public class EmegencyContactActivity extends AppCompatActivity {

    TextView no_contact_info;
    private ContactRecyclerAdapter adapter;
    private static final int PERMS_CONTACT_READ_REQUEST_CODE = 2;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emegency_contact);
        no_contact_info = (TextView) findViewById(R.id.no_contact_info);
        toolbar = (Toolbar) findViewById(R.id.toolbar1); //learnt new thing: from include id, set it to Toolbar
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Emergency Contact Details");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        setUpRecyclerView();

    }


    private void setUpRecyclerView() {

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        List<Contact> myFriendS = Contact.getData(this);
        adapter = new ContactRecyclerAdapter(this, myFriendS);
        recyclerView.setAdapter(adapter);

        showNoContactLogic(myFriendS);

        LinearLayoutManager mLinearLayoutManagerVertical = new LinearLayoutManager(this); // (Context context, int spanCount)
        mLinearLayoutManagerVertical.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(mLinearLayoutManagerVertical);

        recyclerView.setItemAnimator(new DefaultItemAnimator()); // Even if we dont use it then also our items shows default animation. #Check Docs
    }

    private void showNoContactLogic(List<Contact> myFriendS) {
        if (myFriendS == null || myFriendS.size() == 0) {
            no_contact_info.setVisibility(View.VISIBLE);
        } else {
            no_contact_info.setVisibility(View.GONE);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.emergency_conact_page_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.exit:
                finishAndRemoveTask();
                break;
            case R.id.home_menu:
                finish();
                break;
            case R.id.add_contact:
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE);
                startActivityForResult(intent, PERMS_CONTACT_READ_REQUEST_CODE);
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    //Contact added response
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PERMS_CONTACT_READ_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Uri contactData = data.getData();
                Cursor cursor = managedQuery(contactData, null, null, null, null);
                cursor.moveToFirst();

                String phonenumber = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.NUMBER));
                String displayName = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));

                //TODO: save to preferences
                Contact c = new Contact();
                c.setPhoneNumber(phonenumber.replace(" ", "").replace("-", ""));
                c.setContactName(displayName);

                if (adapter != null) {
                    adapter.add(c);
                    no_contact_info.setVisibility(View.GONE);
                }
            }
        }
    }
}
