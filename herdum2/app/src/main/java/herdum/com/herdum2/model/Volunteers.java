package herdum.com.herdum2.model;

public class Volunteers {

    private VolunteerInfo[] devices;

    public void setDevices(VolunteerInfo[] devices) {
        this.devices = devices;
    }

    public VolunteerInfo[] getDevices() {
        return devices;
    }

    public class VolunteerInfo{

        private String lat;
        private String lon;
        private String mobile;
        private String name;

        public void setLat(String lat) {
            this.lat = lat;
        }

        public void setLon(String lon) {
            this.lon = lon;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getLat() {
            return lat;
        }

        public String getLon() {
            return lon;
        }

        public String getMobile() {
            return mobile;
        }

        public String getName() {
            return name;
        }
    }
}
