package herdum.com.herdum2.app;

import android.Manifest;
import android.app.PendingIntent;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.hardware.Camera;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.provider.ContactsContract;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.telephony.SmsManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import herdum.com.herdum2.Constants;
import herdum.com.herdum2.R;
import herdum.com.herdum2.ScreenChangeReceiver;
import herdum.com.herdum2.adapter.ContactRecyclerAdapter;
import herdum.com.herdum2.backend.BluetoothManager;
import herdum.com.herdum2.backend.NetworkManager;
import herdum.com.herdum2.backend.Utility;
import herdum.com.herdum2.model.Contact;
import herdum.com.herdum2.model.Volunteers;


public class MainActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,LocationListener{

    private static final String TAG = "MainActivity";

    Toolbar toolbar;

    public ScreenChangeReceiver screenChangeReceiver;

    private static final int PERMS_SMS_SEND_REQUEST_CODE = 1 ;

    private static final int PERMS_LOCATION_ACCESS_REQUEST_CODE = 3 ;
    private static final int PERMS_REQUEST_CODE =4;
    private BroadcastReceiver sentStatusReceiver, deliveredStatusReceiver;

    private NetworkManager networkManager ;
    private GoogleApiClient mGoogleApiClient;
    private Location mLocation;
    private LocationManager mLocationManager;
    private LocationRequest mLocationRequest;
    private com.google.android.gms.location.LocationListener listener;
    private long UPDATE_INTERVAL = 2 * 1000;  /* 10 secs */
    private long FASTEST_INTERVAL = 2000; /* 2 sec */
    private String latitude;
    private String longititue;

    private ContactRecyclerAdapter adapter;
    TextView no_contact_info;
    private BluetoothManager bluetoothManager;
    private final static int REQUEST_ENABLE_BT = 1;

    private boolean IsLogSendRequired = false;

    private ImageView connected_image;
    private ImageView waiting_for_gps_image;
    private TextView waiting_for_gps_text;
    private TextView connected_image_text;
    private TextView covered_under_text;

    private Handler handler = new Handler(){

        @Override
        public void handleMessage(Message msg) {

            IsLogSendRequired = true;
            sendLog();
            sendMySMS();
            Toast.makeText(MainActivity.this, "Event Received", Toast.LENGTH_LONG).show();
            super.handleMessage(msg);
        }
    };


    private Runnable runnable = new Runnable() {
        @Override
        public void run() {

            fetchVolunteerInfo(null);
            handler.postDelayed(this, 30000);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.d(TAG, "onCreate: " + getIntent().getStringExtra("SEND_DATA"));
        networkManager = NetworkManager.getInstance(this);

        // getIntent().getStringExtra("SEND_DATA");
        toolbar = (Toolbar) findViewById(R.id.toolbar1); //learnt new thing: from include id, set it to Toolbar
        connected_image_text = findViewById(R.id.connected_image_text);
        connected_image = findViewById(R.id.connected_image);
        waiting_for_gps_image = findViewById(R.id.waiting_for_gps_image);
        waiting_for_gps_text = findViewById(R.id.waiting_for_gps_text);
        covered_under_text = findViewById(R.id.covered_under_text);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Emergency");
        registerScreenChangeReceiver();
        if (hasPermissions()) {

        } else {
            requestPerms();
        }

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mLocationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        checkLocation(); //check whether location service is enable or not in your  phone

        ImageView fab =findViewById(R.id.sms_button);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Sending SOS message to your contact", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                IsLogSendRequired = false;
                sendMySMS();
                sendLog();
            }
        });


        if (getIntent().getStringExtra("SEND_DATA") != null && getIntent().getStringExtra("SEND_DATA").equals("send")) {
            //sendLog();
            //sendMySMS();
            IsLogSendRequired = true;
            Log.d(TAG, "onCreate: "+"OPNED DUE TO ACTION_USER_PRESENT");
        }
        initializeBluetooth();
        handler.post(runnable);
    }
    private boolean success = false;
    private void sendLog() {

        Toast.makeText(MainActivity.this, "sending latitude : "
                + latitude + " and longitude : " + longititue, Toast.LENGTH_LONG).show();

        networkManager = NetworkManager.getInstance(this);
        final HashMap<String,String> map= Utility.loadContactNameAndPhoneMap(this);
        new Thread(new Runnable() {
            @Override
            public void run() {
                if(map!=null && map.size()>0) {
                    List<String> phoneNumbers = new ArrayList<>();
                    for (String phoneNumber : map.keySet()) {
                        phoneNumbers.add(phoneNumber);
                    }
                    success = networkManager.postData1(latitude, longititue, phoneNumbers);
                    Log.d(TAG, "Send Log: "+latitude+" "+ longititue + "total contact in log "+map.size());

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if(success)
                                Toast.makeText(MainActivity.this, "successfully sent to server", Toast.LENGTH_LONG).show();
                            else
                                Toast.makeText(MainActivity.this, "unable to sent to server", Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }
        }).start();


    }

    public void sendMySMS() {
        Toast.makeText(this, "SENDING HELP REQUEST", Toast.LENGTH_LONG).show();
        HashMap<String,String> map= Utility.loadContactNameAndPhoneMap(this);
        SmsManager sms = SmsManager.getDefault();
        sendSMSToMyFriends(map, sms);
        sendSMSToAdmin(sms);
        sendSMSToNearByVolunteers(sms);
    }

    private void sendSMSToAdmin(SmsManager sms) {
        for(String phoneNumber: Constants.DEFAULT_CONTACTS.keySet()){
            String phone = phoneNumber;
            String message = Constants.FIX_SMS;
            //Check if the phoneNumber is empty
            if (phone.isEmpty()) {
              //  Toast.makeText(this, "Please Enter a Valid Phone Number", Toast.LENGTH_SHORT).show();
            } else {

                Log.d(TAG, "Send sms: "+ phone);

                // if message length is too long messages are divided
                List<String> messages = sms.divideMessage(message);
                for (String msg : messages) {

                    PendingIntent sentIntent = PendingIntent.getBroadcast(this, 0, new Intent("SMS_SENT"), 0);
                    PendingIntent deliveredIntent = PendingIntent.getBroadcast(this, 0, new Intent("SMS_DELIVERED"), 0);
                    sms.sendTextMessage(phone, null, msg, sentIntent, deliveredIntent);

                }
            }
        }
    }

    private void sendSMSToMyFriends(HashMap<String, String> map, SmsManager sms) {
        if(map!=null &&  map.size()>-0) {
            for (String phoneNumber : map.keySet()) {
                String phone = phoneNumber;
                String message = "help !!! help!!!";
                if(!TextUtils.isEmpty(latitude)&& !TextUtils.isEmpty(longititue)){
                   message = String.format("help!!! help !!! my location- http://maps.google.com/maps?q=%s,%s",latitude,longititue);
                }
                //Check if the phoneNumber is empty
                if (phone.isEmpty()) {
                    Toast.makeText(this, "Please Enter a Valid Phone Number", Toast.LENGTH_SHORT).show();
                } else {
                    Log.d(TAG, "Send sms: "+ phone);
                    // if message length is too long messages are divided
                    List<String> messages = sms.divideMessage(message);
                    for (String msg : messages) {

                        PendingIntent sentIntent = PendingIntent.getBroadcast(this, 0, new Intent("SMS_SENT"), 0);
                        PendingIntent deliveredIntent = PendingIntent.getBroadcast(this, 0, new Intent("SMS_DELIVERED"), 0);
                        sms.sendTextMessage(phone, null, msg, sentIntent, deliveredIntent);

                    }
                }
            }
        }
    }

    private void sendSMSToNearByVolunteers(SmsManager smsManager){

        Toast.makeText(MainActivity.this, "sending latitude : "
                + latitude + " and longitude : " + longititue, Toast.LENGTH_LONG).show();


        fetchVolunteerInfo(smsManager);
    }

    private void fetchVolunteerInfo(final SmsManager smsManager){

        new Thread(new Runnable() {
            @Override
            public void run() {

                Volunteers.VolunteerInfo[] volunteers = networkManager.getVolunteersInfo().getDevices();

                String deviceNames = "";
                for(Volunteers.VolunteerInfo volunteerInfo : volunteers){

                    try{

                        double vLat = Double.parseDouble(volunteerInfo.getLat());
                        double vLon = Double.parseDouble(volunteerInfo.getLon());
                        double myLat = Double.parseDouble(latitude);
                        double myLon = Double.parseDouble(longititue);

                        double distance = distance(vLat, vLon, myLat, myLon);
                        if(distance <= 50000.0) {

                            if(TextUtils.isEmpty(deviceNames)){
                                deviceNames = volunteerInfo.getName();
                            }
                            else {
                                deviceNames = deviceNames + ", " + volunteerInfo.getName();
                            }
                            if(smsManager != null) {
                                sendSMSToVolunteerIfRequired(volunteerInfo, smsManager);
                            }
                        }
                    }
                    catch (Exception e){

                        Log.e(TAG, "Exception in sendSMSToVolunteerIfRequired: unable to parse latitude.longitude");
                    }
                }

                notifyCoveredUnderText(deviceNames);
            }
        }).start();
    }

    private void notifyCoveredUnderText(final String deviceNames){

        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if(!TextUtils.isEmpty(deviceNames)) {
                    String message = "YOU ARE COVERED UNDER SENTINEL [ " + deviceNames + " ]";

                    covered_under_text.setText(message);
                    covered_under_text.setVisibility(View.VISIBLE);
                }
                else{
                    covered_under_text.setVisibility(View.GONE);

                }
            }
        });
    }

    private void sendSMSToVolunteerIfRequired(Volunteers.VolunteerInfo volunteerInfo, SmsManager smsManager){

        try{

            PendingIntent sentIntent = PendingIntent.getBroadcast(this, 0, new Intent("SMS_SENT"), 0);
            PendingIntent deliveredIntent = PendingIntent.getBroadcast(this, 0, new Intent("SMS_DELIVERED"), 0);
            smsManager.sendTextMessage(volunteerInfo.getMobile(), null,"abc 123 setdigout 1 10", sentIntent, deliveredIntent);
        }
        catch (Exception e){

            Log.e(TAG, "Exception in sendSMSToVolunteerIfRequired: unable to parse latitude.longitude");
        }

    }

    public static double distance(double lat1, double lon1, double lat2, double lon2) {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        dist = dist * 1.609344;
        return (dist * 1000);
    }

    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    /*::  This function converts decimal degrees to radians             :*/
    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    public static double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    /*::  This function converts radians to decimal degrees             :*/
    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    public static double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }

    @Override
    public void onConnected(Bundle bundle) {


        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        startLocationUpdates();
        mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if (mLocation == null) {
            startLocationUpdates();
        }
        if (mLocation != null) {

            // mLatitudeTextView.setText(String.valueOf(mLocation.getLatitude()));
            //mLongitudeTextView.setText(String.valueOf(mLocation.getLongitude()));
            showConnected();
        } else {
            Toast.makeText(this, "Location not Detected", Toast.LENGTH_SHORT).show();
            showWaitingForGPS();
        }


    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.i(TAG, "Connection Suspended");
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.i(TAG, "Connection failed. Error: " + connectionResult.getErrorCode());
    }



    @Override
    protected void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    protected void startLocationUpdates() {
        // Create the location request
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(UPDATE_INTERVAL)
                .setFastestInterval(FASTEST_INTERVAL);
        // Request location updates

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,
                mLocationRequest, this);
        Log.d("reque", "--->>>>");
    }

    private void showWaitingForGPS() {
        waiting_for_gps_image.setVisibility(View.VISIBLE);
        waiting_for_gps_text.setVisibility(View.VISIBLE);
        connected_image_text.setVisibility(View.GONE);
        connected_image.setVisibility(View.GONE);
    }

    private void showConnected() {
        waiting_for_gps_image.setVisibility(View.GONE);
        waiting_for_gps_text.setVisibility(View.GONE);
        connected_image_text.setVisibility(View.VISIBLE);
        connected_image.setVisibility(View.VISIBLE);
    }

    @Override
    public void onLocationChanged(Location location) {

        String msg = "Updated Location: " +
                Double.toString(location.getLatitude()) + "," +
                Double.toString(location.getLongitude());
        Log.d(TAG, "onLocationChanged: lat-"+String.valueOf(location.getLatitude()));
        Log.d(TAG, "onLocationChanged: long-"+String.valueOf(location.getLongitude()));
        latitude = String.valueOf(location.getLatitude());
        longititue =String.valueOf(location.getLongitude());
        if(IsLogSendRequired){
            IsLogSendRequired = false;
            sendLog();
            sendMySMS();

        }
       // Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        // You can now create a LatLng Object for use with maps
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
    }

    private boolean checkLocation() {
        if(!isLocationEnabled())
            showAlert();
        else{
            showConnected();
        }
        return isLocationEnabled();
    }

    private void showAlert() {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Enable Location")
                .setMessage("Your Locations Settings is set to 'Off'.\nPlease Enable Location to " +
                        "use this app")
                .setPositiveButton("Location Settings", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {

                        Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(myIntent);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {

                    }
                });
        dialog.show();
    }

    private boolean isLocationEnabled() {
        mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        return mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) ||
                mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    private void registerScreenChangeReceiver() {
        IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_ON);
        filter.addAction(Intent.ACTION_SCREEN_OFF);
        filter.addAction(Intent.ACTION_USER_PRESENT);
        screenChangeReceiver = new ScreenChangeReceiver();
        registerReceiver(screenChangeReceiver, filter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (screenChangeReceiver != null)
        {
            unregisterReceiver(screenChangeReceiver);
            screenChangeReceiver = null;
        }

        if(bluetoothManager != null){
            bluetoothManager.close();
        }
    }


    //MENU
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
        }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.exit:
                finishAndRemoveTask();
                break;
            case R.id.profile_menu:
                Intent my_profile_intent = new Intent(MainActivity.this, MyProfileActivity.class);
                startActivity(my_profile_intent);
                break;
            case R.id.emergency_menu:
                Intent emergency_contact_intent = new Intent(MainActivity.this, EmegencyContactActivity.class);
                startActivity(emergency_contact_intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }
    //PERMISSION
    private boolean hasPermissions(){
        int res =0;
        String[] permissions = new String[]{Manifest.permission.READ_CONTACTS,Manifest.permission.SEND_SMS,Manifest.permission.ACCESS_COARSE_LOCATION,Manifest.permission.ACCESS_FINE_LOCATION};
        for(String perm : permissions)
        {
            res = checkCallingOrSelfPermission(perm);
            if(!(res == PackageManager.PERMISSION_GRANTED))
            {
                return false;
            }
        }
        return true;
    }
    private void requestPerms(){
        String[] permissions = new String[]{Manifest.permission.READ_CONTACTS,Manifest.permission.SEND_SMS,Manifest.permission.ACCESS_COARSE_LOCATION,Manifest.permission.ACCESS_FINE_LOCATION};

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(permissions,PERMS_REQUEST_CODE);
        }

    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        boolean allowed = true;
        switch (requestCode)
        {
            case PERMS_REQUEST_CODE:
                for(int res:grantResults)
                {
                    allowed = allowed && (res == PackageManager.PERMISSION_GRANTED);
                }
                break;
            default:

                allowed = false;
                break;

        }
        if(allowed){
           //granted
        }
        else{
            //we will give warning to user that they haven't granted permission.
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (shouldShowRequestPermissionRationale(Manifest.permission.READ_CONTACTS)) {
                    Toast.makeText(this, "Please provide read contact permission from settings", Toast.LENGTH_SHORT).show();
                }
                if (shouldShowRequestPermissionRationale(Manifest.permission.SEND_SMS)) {
                    Toast.makeText(this, "Please provide read/send sms permission from settings", Toast.LENGTH_SHORT).show();
                }
                if (shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_COARSE_LOCATION)) {
                    Toast.makeText(this, "Please provide location from settings", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }


    //Contact added response
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_ENABLE_BT) {

            if(resultCode == RESULT_OK){
                bluetoothManager.start();
            }
            else{

                Toast.makeText(this, "Enable bluettoth to use this App", Toast.LENGTH_LONG).show();
            }
        }
    }



    private void initializeBluetooth(){
        BluetoothManager.initialize(this, handler);
        bluetoothManager = BluetoothManager.getInstance();
        enableBluetooth();
        markDiscoverable();
    }

    void markDiscoverable(){

        Intent discoverableIntent =
                new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
        discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 3500);
        startActivity(discoverableIntent);
    }

    private void enableBluetooth(){
        if(!bluetoothManager.isEnabled()){

            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }
        else{
            bluetoothManager.start();
        }
    }

    static int flashCount =0;
    CameraManager camManager;
    String cameraId;
    public void FlashClick(View view) {
        Log.d(TAG, "FlashClick: ");
        if(this.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH)){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if(flashCount==0) {
                    CameraManager camManager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
                    String cameraId = null; // Usually back camera is at 0 position.
                    try {
                        cameraId = camManager.getCameraIdList()[0];
                        while(true) {
                            camManager.setTorchMode(cameraId, true);   //Turn ON
                            Thread.sleep(500);
                            camManager.setTorchMode(cameraId, false);   //Turn OFF
                            Thread.sleep(500);

                            flashCount = flashCount+1;
                            if(flashCount==5){
                                flashCount=0;
                                break;
                            }
                        }
                    } catch (CameraAccessException e) {
                        e.printStackTrace();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

  /*  public static Camera cam = null;
    public void flashLightOn(View view) {

        try {
            if (getPackageManager().hasSystemFeature(
                    PackageManager.FEATURE_CAMERA_FLASH)) {
                cam = Camera.open();
                Parameters p = cam.getParameters();
                p.setFlashMode(Parameters.FLASH_MODE_TORCH);
                cam.setParameters(p);
                cam.startPreview();
            }
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(getBaseContext(), "Exception flashLightOn()",
                    Toast.LENGTH_SHORT).show();
        }
    }

    public void flashLightOff(View view) {
        try {
            if (getPackageManager().hasSystemFeature(
                    PackageManager.FEATURE_CAMERA_FLASH)) {
                cam.stopPreview();
                cam.release();
                cam = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(getBaseContext(), "Exception flashLightOff",
                    Toast.LENGTH_SHORT).show();
        }
    }*/
}
