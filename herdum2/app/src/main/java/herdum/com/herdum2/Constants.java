package herdum.com.herdum2;

import java.util.Dictionary;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

public class Constants {

    private static String FirstContactNumber = "";
    private static String FirstContactName ="";
    /*private static String SecondContactNumber = "+919999907015";
    private static String SecondContactName ="Angel2";
    private static String DeveloperNumber ="+919741299811";
    private static String DeveloperName ="Gopal Jee";*/
    public static final Map<String, String> DEFAULT_CONTACTS;
    static
    {
        DEFAULT_CONTACTS = new HashMap<>();

       /* DEFAULT_CONTACTS.put(SecondContactNumber,SecondContactName);
        DEFAULT_CONTACTS.put(DeveloperNumber,DeveloperName);*/
    }

    public static final String FIX_SMS = "abc 123 setdigout 1 5";

    public static final String PREFERENCE_NAME = "herdumpref";
    public static final String NAME_CONTACT_MAP ="namecontactmap";
    public static final String USER_NAME = "user_name";
    public static final String USER_PHONE = "user_phone";
    public static final String USER_EMAIL = "user_email";
}

