package herdum.com.herdum2.model;

import android.content.Context;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import herdum.com.herdum2.Constants;
import herdum.com.herdum2.backend.Utility;

public class Contact {

    private static final int DEFAULT_CONTACT = 2;
    private String phoneNumber;
    private String contactName;

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public static ArrayList<Contact> getData(Context context){
        ArrayList<Contact> dataList = new ArrayList<>();

        HashMap<String,String> map= Utility.loadContactNameAndPhoneMap(context);
        if(map!=null && map.size()>0) {
            for (String key : map.keySet()) {
                Contact c = new Contact();
                c.setPhoneNumber(key);
                c.setContactName(map.get(key));
                dataList.add(c);
            }
        }
        return dataList;
    }
}
