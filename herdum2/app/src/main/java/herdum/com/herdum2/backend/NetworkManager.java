package herdum.com.herdum2.backend;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;
import java.util.List;

import herdum.com.herdum2.model.Volunteers;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.Response;


public class NetworkManager {

    private final static String TAG = NetworkManager.class.getSimpleName();
    public final String HOST_URL ="http://104.211.101.35:8080/SmsSender-0.0.1/mmi/smsservice/test";
    private Context mContext;
    public static final MediaType JSON
            = MediaType.parse("application/json; charset=utf-8");

    OkHttpClient client = new OkHttpClient();
    //it's okay because it's application Context
    private static NetworkManager mInstance;
    private NetworkManager(Context context) {

        mContext = context;

        Log.d(TAG, "NetworkManager HOST_URL: " + HOST_URL);
    }


    public static NetworkManager getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new NetworkManager(context.getApplicationContext());
        }
        return mInstance;
    }
    public boolean postData1(String lattitude,String longitude,List<String> phoneNumbers) {

        try {
            JSONObject data = new JSONObject();
            data.put("lat", lattitude);
            data.put("lon", longitude);
            data.put("time", String.valueOf(System.currentTimeMillis()/1000));
            data.put("mobileNo", new JSONArray(phoneNumbers));
            Log.d(TAG, "body: request body is " + data.toString());

            RequestBody body = RequestBody.create(JSON,data.toString());
            okhttp3.Request request = new okhttp3.Request.Builder()
                    .url(HOST_URL)
                    .post(body)
                    .addHeader("Content-Type", "application/json")
                    .addHeader("cache-control", "no-cache")

                    .build();
            Response response = client.newCall(request).execute();
            Log.d(TAG, "Response success : " + response.isSuccessful());
            return response.isSuccessful();
        } catch (Exception ex) {
            Log.e(TAG, "postData1: ",ex);
        }
        return false;
    }

    public Volunteers getVolunteersInfo() {

        try {

            okhttp3.Request request = new okhttp3.Request.Builder()
                    .url("http://104.211.101.35:8080/SmsSender-0.0.1/mmi/smsservice/location")
                    .get()
                    .addHeader("Content-Type", "application/json")
                    .addHeader("cache-control", "no-cache")
                    .build();
            Response response = client.newCall(request).execute();
            String json = response.body().string();

            Log.d(TAG, "Response body is : " + json);
            return new Gson().fromJson(json, Volunteers.class);
        } catch (Exception ex) {
            Log.e(TAG, "postData1: ",ex);
        }
        return null;
    }


    public void close() {
        mContext = null;
        mInstance = null;
    }
}
