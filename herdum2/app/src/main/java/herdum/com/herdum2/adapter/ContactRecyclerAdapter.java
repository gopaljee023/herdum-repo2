package herdum.com.herdum2.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import herdum.com.herdum2.R;
import herdum.com.herdum2.backend.Utility;
import herdum.com.herdum2.model.Contact;

public class ContactRecyclerAdapter extends RecyclerView.Adapter<ContactRecyclerAdapter.ContactViewHolder> {
    List<Contact> mData;
    private LayoutInflater inflater;
    Context context;

    public ContactRecyclerAdapter(Context context, List<Contact> data) {
        inflater = LayoutInflater.from(context);
        this.mData = data;
        this.context = context;
        saveToPreference();
    }

    @Override
    public ContactViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.contact_list, parent, false);
        ContactViewHolder holder = new ContactViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ContactViewHolder holder, final int position) {
        Contact current = mData.get(position);
        holder.img_row_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mData.remove(position);
                saveToPreference();
                notifyDataSetChanged();
            }
        });
        holder.setData(current, position);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public void add(Contact c) {
        mData.add(c);
        saveToPreference();
        notifyItemInserted(mData.size()-1);
    }

    private void saveToPreference() {
        HashMap<String,String> map = new HashMap<>();
        for(int i=0;i<mData.size();i++){
            //TODO:fail in case of same contact name
            map.put(mData.get(i).getPhoneNumber(),mData.get(i).getContactName());
        }
        Utility.saveContactNameAndPhoneMap(context,map);
    }

    class ContactViewHolder extends RecyclerView.ViewHolder {
        TextView tvName;
        TextView tvPhonenumber;
        ImageView img_row_delete;
        int position;
        Contact current;

        public ContactViewHolder(View itemView) {
            super(itemView);
            tvName   = (TextView)  itemView.findViewById(R.id.tvName);
            tvPhonenumber = (TextView)  itemView.findViewById(R.id.tvPhonenumber);
            img_row_delete = (ImageView)itemView.findViewById(R.id.img_row_delete);
        }

        public void setData(Contact current, int position) {
            this.tvName.setText(current.getContactName());
            this.tvPhonenumber.setText(current.getPhoneNumber());
            this.position = position;
            this.current = current;
        }
    }
}
