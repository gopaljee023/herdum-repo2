package herdum.com.herdum2.backend;

import android.content.Context;
import android.content.SharedPreferences;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;


import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.UUID;

import herdum.com.herdum2.Constants;

public final class Utility {

    private static String TAG = Utility.class.getSimpleName();

    private static final int MAX_LENGTH = 30;
    private static SharedPreferences mSharedPreferences;
    private static SharedPreferences getSharedPreference(Context context){
        if(mSharedPreferences == null){
            mSharedPreferences = context.getSharedPreferences(Constants.PREFERENCE_NAME, Context.MODE_PRIVATE);
        }
        return mSharedPreferences;
    }


    public static boolean convertStringToBoolean(String admin_invite_only_status_of_group) {
        Boolean returningBool = true;
        if(TextUtils.isEmpty(admin_invite_only_status_of_group) || !admin_invite_only_status_of_group.equals("1")){
            returningBool  = false;
        }
        return returningBool;
    }


    public static HashMap<String,String> loadContactNameAndPhoneMap(Context context){
        HashMap<String,String> map = new HashMap<>();
        SharedPreferences preference = getSharedPreference(context);
        String data = preference.getString(Constants.NAME_CONTACT_MAP, "");
        if (!TextUtils.isEmpty(data)){
            Type type = new TypeToken<HashMap<String,String>>(){}.getType();
            map = new Gson().fromJson(data,type);
        }
        return map;
    }
    //phone number vs contact.
    public static void saveContactNameAndPhoneMap(Context context, HashMap<String,String> map){
        SharedPreferences preference = getSharedPreference(context);
        preference.edit()
                .putString(Constants.NAME_CONTACT_MAP,new Gson().toJson(map))
                .apply();
    }

    public static void saveUserName(Context context, String name){
        SharedPreferences preference = getSharedPreference(context);
        preference.edit()
                .putString(Constants.USER_NAME,name)
                .apply();
    }


    public static void saveUserPhone(Context context, String phone){
        SharedPreferences preference = getSharedPreference(context);
        preference.edit()
                .putString(Constants.USER_PHONE,phone)
                .apply();
    }

    public static void saveUserEmail(Context context, String email){
        SharedPreferences preference = getSharedPreference(context);
        preference.edit()
                .putString(Constants.USER_EMAIL,email)
                .apply();
    }

    public static String getPhoneNumber(Context context){
        SharedPreferences preference = getSharedPreference(context);
        return preference.getString(Constants.USER_PHONE, "");
    }

    public static String getUserName(Context context){
        SharedPreferences preference = getSharedPreference(context);
        return preference.getString(Constants.USER_NAME, "");
    }
    public static String getUserEmail(Context context){
        SharedPreferences preference = getSharedPreference(context);
        return preference.getString(Constants.USER_EMAIL, "");
    }

}
