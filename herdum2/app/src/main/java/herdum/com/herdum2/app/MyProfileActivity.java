package herdum.com.herdum2.app;

import android.content.Intent;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import herdum.com.herdum2.R;
import herdum.com.herdum2.backend.Utility;

public class MyProfileActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private EditText edit_phone;
    private EditText edit_name;
    private EditText edit_email;
    private MenuItem saveItem;
    private MenuItem editItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile);
        toolbar = (Toolbar) findViewById(R.id.toolbar1); //learnt new thing: from include id, set it to Toolbar
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Profile");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        edit_phone = findViewById(R.id.edit_phone);
        edit_name = findViewById(R.id.edit_name);
        edit_email = findViewById(R.id.edit_email);
        setProfileData();
    }

    private void setProfileData() {
        String phone = Utility.getPhoneNumber(this);
        String name = Utility.getUserName(this);
        String email = Utility.getUserEmail(this);
        if (!TextUtils.isEmpty(phone)) {
            edit_phone.setText(phone);
        }
        if (!TextUtils.isEmpty(name)) {
            edit_name.setText(name);
        }
        if (!TextUtils.isEmpty(email)) {
            edit_email.setText(email);
        }

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.user_profile_page_menu, menu);
        saveItem = menu.findItem(R.id.save);
        if (saveItem != null) {
            saveItem.setVisible(false);
        }
        editItem = menu.findItem(R.id.edit);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.exit:
                finishAndRemoveTask();
                break;
            case R.id.home_menu:
                finish();
                break;
            case R.id.edit:
                edit_phone.setEnabled(true);
                edit_email.setEnabled(true);
                edit_name.setEnabled(true);
                item.setVisible(false);
                saveItem.setVisible(true);
                break;
            case R.id.save:
                edit_phone.setEnabled(false);
                edit_email.setEnabled(false);
                edit_name.setEnabled(false);
                item.setVisible(false);
                editItem.setVisible(true);
                //TODO:save to preferences
                saveProfileData();
                setProfileData();

        }
        return super.onOptionsItemSelected(item);
    }

    private void saveProfileData() {
        String phone = edit_phone.getText().toString();
        String name = edit_name.getText().toString();
        String email = edit_email.getText().toString();
        if (!TextUtils.isEmpty(email)) {
            Utility.saveUserEmail(this, email);
        }
        if (!TextUtils.isEmpty(name)) {
            Utility.saveUserName(this, name);
        }
        if (!TextUtils.isEmpty(phone)) {
            Utility.saveUserPhone(this, phone);
        }

    }
}
