package herdum.com.herdum2;

import android.app.KeyguardManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.SmsManager;
import android.util.Log;
import android.widget.Toast;

import herdum.com.herdum2.backend.NetworkManager;
import herdum.com.herdum2.backend.Utility;
import herdum.com.herdum2.model.Contact;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ScreenChangeReceiver extends BroadcastReceiver {
    private static int countPowerOff = 0;
    public ScreenChangeReceiver()
    {

    }

    static long firstPressedTime = 0;

    public void onReceive(Context context, Intent intent)
    {
         if(firstPressedTime==0){
             firstPressedTime = System.currentTimeMillis();
         }
        if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF))
        {
            Log.d("In on receive", "In Method:  ACTION_SCREEN_OFF");
            countPowerOff++;
        }
        else if (intent.getAction().equals(Intent.ACTION_SCREEN_ON))
        {
            Log.d("In on receive", "In Method:  ACTION_SCREEN_ON");
            countPowerOff++;

        }
        else if(intent.getAction().equals(Intent.ACTION_USER_PRESENT)) {
            KeyguardManager manager = (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);
            if (manager.isKeyguardLocked()) {
                Log.d("MyReceiver", "Locked");
            }
            Log.d("In on receive", "In Method:  ACTION_USER_PRESENT");
            long timepassedInSeconds = (System.currentTimeMillis() - firstPressedTime)/1000;
            Log.d("TIME PASSED", "onReceive: "+timepassedInSeconds);
            if(timepassedInSeconds>=8){
                countPowerOff = 0;
                firstPressedTime=0;
            }
            else if(countPowerOff > 5) {
                countPowerOff = 0;
                firstPressedTime =0;
                Intent i = context.getPackageManager()
                        .getLaunchIntentForPackage(context.getPackageName() );

                i.putExtra("SEND_DATA","send");
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK );
                context.startActivity(i);
            }
        }

    }
}
