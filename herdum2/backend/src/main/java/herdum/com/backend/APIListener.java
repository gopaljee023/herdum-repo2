package herdum.com.backend;

import android.text.TextUtils;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public abstract class APIListener implements Response.Listener, Response.ErrorListener{
    class ErrorResponse {

        @SerializedName("errorCode")
        @Expose
        private Integer errorCode;
        @SerializedName("errorMsg")
        @Expose
        private String errorMsg;

        public Integer getErrorCode() {
            return errorCode;
        }

        public void setErrorCode(Integer errorCode) {
            this.errorCode = errorCode;
        }

        public String getErrorMsg() {
            return errorMsg;
        }

        public void setErrorMsg(String errorMsg) {
            this.errorMsg = errorMsg;
        }

    }

    public static final String TAG = APIListener.class.getSimpleName();
    @Override
    public void onErrorResponse(VolleyError error) {
        String errorMessaage=null;
        try {
            String json = new String(
                    error.networkResponse.data,
                    HttpHeaderParser.parseCharset(error.networkResponse.headers));
            Log.d(TAG, "onErrorResponse: "+json);
          ErrorResponse errorResponse =  new Gson().fromJson(json,ErrorResponse.class);
            if(!TextUtils.isEmpty(errorResponse.errorMsg)){
                errorMessaage = errorResponse.errorMsg;
            }
        } catch (Exception e) {

        }
        if(TextUtils.isEmpty(errorMessaage)) {
            if (error instanceof TimeoutError) {
                errorMessaage = "Communication Error!";
            }
            else if (error instanceof NoConnectionError) {
                errorMessaage = "Network Issue";
            }
            else if (error instanceof AuthFailureError) {
                errorMessaage = "Authentication Error!";
            } else if (error instanceof ServerError) {
                errorMessaage = "Server Side Error!";
            } else if (error instanceof NetworkError) {
                errorMessaage = "Network Error!";
            } else if (error instanceof ParseError) {
                errorMessaage = "Parse Error!";
            }
        }
        onAPIFailure(errorMessaage);
    }

    @Override
    public void onResponse(Object response) {
        onAPISuccess(response);
    }

    //method for send response to UI

    public abstract void onAPISuccess(Object response);

    public abstract void onAPIFailure(String message);
}
