package herdum.com.backend;

import android.content.Context;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import herdum.com.backend.model.ResponseModel;


public class NetworkManager {

    private final static String TAG = NetworkManager.class.getSimpleName();
    private final static String AUTHORIZATION_KEY = "Authorization";
    private final static String CONTENT_TYPE_KEY = "Content-Type";
    private final static String CONTENT_TYPE_VALUE = "application/json";
    public final String HOST_URL ="https://demoapp1812.azurewebsites.net/api/Function1";

    private Context mContext;
    private static RequestQueue mRequestQueue;
    private static Map<String, String> mDefaultHeaders;

    //it's okay because it's application Context
    private static NetworkManager mInstance;
    private String basicAuth;

    private NetworkManager(Context context) {

        mContext = context;

        Log.d(TAG, "NetworkManager HOST_URL: " + HOST_URL);

        mRequestQueue = Volley.newRequestQueue(mContext);
       mDefaultHeaders = new HashMap<>();
        mDefaultHeaders.put(CONTENT_TYPE_KEY, CONTENT_TYPE_VALUE);
    }

    public static NetworkManager getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new NetworkManager(context.getApplicationContext());
        }
        return mInstance;
    }

    private void addRequest(Request request) {
        mRequestQueue.add(request);
    }

    public void cancelRequest(String tag) {
        mRequestQueue.cancelAll(tag);
    }

    public void postData(String lat,String longitute,List<String> phoneNumbers, APIListener listner){
        try {
            JSONObject data = new JSONObject();
            data.put("mobileNo", new JSONArray(phoneNumbers));
            data.put("lat", lat);
            data.put("lon", longitute);
            data.put("time", String.valueOf(System.currentTimeMillis()/1000));

            Log.d(TAG, "registerUser: request body is " + data.toString());

            GsonRequest request = new GsonRequest(Request.Method.POST,
                    HOST_URL,
                    ResponseModel.class, mDefaultHeaders, data, listner, listner);

            addRequest(request);
        } catch (Exception e) {
            e.printStackTrace();
            listner.onAPIFailure("Exception in registerUser");
            Log.d(TAG, "Exception in registerUser " + e.getMessage());
        }
    }

    public void close() {
        mRequestQueue.stop();
        mRequestQueue = null;
        mContext = null;
        mInstance = null;
    }
}
